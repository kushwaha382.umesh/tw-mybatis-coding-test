CREATE TABLE users(
  id varchar(50) NOT NULL,
  created_by varchar(50) ,
  created_time timestamp DEFAULT CURRENT_TIMESTAMP 
NOT NULL,
  last_updated_by varchar(50),
  last_updated_time timestamp DEFAULT CURRENT_TIMESTAMP 
NOT NULL,
  first_name varchar(500),
  last_name varchar(500) ,
  picture varchar(500) ,
  preferred_language varchar(100),
  background_check int,
  primary key(id)
)
;