package com.umesh;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MetaDesignAssignmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(MetaDesignAssignmentApplication.class, args);
	}

}
