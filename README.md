# tw-mybatis-coding-test

This is a coding test for tw backend developer candidates.

#Objective
Use [Mybatis Mapper](http://www.mybatis.org/mybatis-3) to write a basic CRUD application on a User table(users.sql) against a postgres database.

# Instructions to code
*  This repo is readonly, so fork this repo on gitlab and create new repo with public access
*  use spring-boot and maven to give structure to the application
*  use mybatis code-generator to auto-generate the mapping code
*  use junit to write a basic test suite for the CRUD operations
*  update the readme to add accurate instructions on how to setup and run the project and run the tests
*  Share the forked repo link with the hr

#Instructions to run 
TODO by candidate
# I used H2 inmemory database, import the application in any IDE and run, and use below rest end point for crud operation

# Or create a jar file using below command 
# mvn clean install
# run the jar file using below command
# java --jar filename
# then use postman or any restclient to test

GET http://localhost:8081/users/1

{
    "id": "1",
    "created_by": "umesh",
    "created_time": "2020-03-26T09:02:32.000+0000",
    "lastUpdated_by": null,
    "lastUpdated_time": null,
    "first_name": "umesh",
    "last_name": "kushwaha",
    "picture": "/umesh",
    "preferred_language": "english",
    "background_check": 1
}


GET http://localhost:8081/users


[
    {
        "id": "1",
        "created_by": "umesh",
        "created_time": "2020-03-26T09:02:32.000+0000",
        "lastUpdated_by": null,
        "lastUpdated_time": null,
        "first_name": "umesh",
        "last_name": "kushwaha",
        "picture": "/umesh",
        "preferred_language": "english",
        "background_check": 1
    },
    {
        "id": "2",
        "created_by": "umesh",
        "created_time": "2020-03-26T09:02:54.000+0000",
        "lastUpdated_by": null,
        "lastUpdated_time": null,
        "first_name": "rakesh",
        "last_name": "kushwaha",
        "picture": "/rakesh",
        "preferred_language": "english",
        "background_check": 2
    },
    {
        "id": "3",
        "created_by": "umesh",
        "created_time": "2020-03-26T09:03:10.000+0000",
        "lastUpdated_by": null,
        "lastUpdated_time": null,
        "first_name": "suresh",
        "last_name": "kushwaha",
        "picture": "/suresh",
        "preferred_language": "english",
        "background_check": 2
    },
    {
        "id": "4",
        "created_by": "umesh",
        "created_time": "2020-03-26T09:03:37.000+0000",
        "lastUpdated_by": null,
        "lastUpdated_time": null,
        "first_name": "sujeet",
        "last_name": "kushwaha",
        "picture": "/sujeet",
        "preferred_language": "english",
        "background_check": 2
    }
]



PUT http://localhost:8081/users/1

 {
        "first_name": "umesh132131",
        "last_name": "kushwaha"
       
    }
    
Delete http://localhost:8081/users/1


POST http://localhost:8081/users

 {
        "id": "7",
        "created_by": "umesh",
        "created_time": "2020-03-26T09:02:32.000+0000",
        "lastUpdated_by": null,
        "lastUpdated_time": null,
        "first_name": "umesh",
        "last_name": "kushwaha",
        "picture": "/umesh",
        "preferred_language": "english",
        "background_check": 1
    }
