package com.umesh.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.umesh.domain.Users;
import com.umesh.exception.UserNotFoundException;
import com.umesh.service.UserService;

@RestController
@RequestMapping("/users")
public class UsersController {
	
	@Autowired
	private UserService userService;

	@GetMapping
	public List<Users> getUsers(){
		return userService.findAll();
	}
	
	@GetMapping("/{id}")
	public Users getUserById(@PathVariable("id") String id)throws UserNotFoundException{
		return userService.findById(id);
	}
	@DeleteMapping("/{id}")
	public HttpStatus deleteById(@PathVariable("id") String id) throws UserNotFoundException{
		userService.deleteById(id);
		
		return HttpStatus.OK;
		
	}
	
	@PutMapping("/{id}")
	public Users updateUser( Users user, @PathVariable("id") String id)throws UserNotFoundException {
		user.setId(id);
		userService.update(user);
		return user;
	}
	
	@PostMapping
	public Users addUser( @RequestBody Users user) {
		
		userService.insert(user);
		return user;
	}
	
}
