package com.umesh.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.umesh.domain.Users;
import com.umesh.exception.UserNotFoundException;
import com.umesh.mapper.UserMapper;

@Service
public class UserServiceImpl implements UserService{

	@Autowired
	private UserMapper userMapper;

	@Override
	public List<Users> findAll() {
		
		return userMapper.findAll();
	}

	@Override
	public Users findById(String id)throws UserNotFoundException {
		Users users = userMapper.findById(id);
		if(users == null) throw new UserNotFoundException();
		return users;
	}

	@Override
	public void deleteById(String id) throws UserNotFoundException{
		int count = userMapper.deleteById(id);
		if(count ==0) {
			throw new UserNotFoundException();
		}
	}

	@Override
	public int insert(Users user) {
		
		return userMapper.insert(user);
	}
	
	

	@Override
	public int update(Users users) {
		
		return userMapper.update(users);
	}
}
