/*
-- Query: select * from users
LIMIT 0, 1000

-- Date: 2020-03-26 14:34
*/
INSERT INTO `users` (`id`,`created_by`,`created_time`,`last_updated_by`,`last_updated_time`,`first_name`,`last_name`,`picture`,`preferred_language`,`background_check`) VALUES ('1','umesh','2020-03-26 14:32:32','umesh','2020-03-26 14:32:32','umesh','kushwaha','/umesh','english',1);
INSERT INTO `users` (`id`,`created_by`,`created_time`,`last_updated_by`,`last_updated_time`,`first_name`,`last_name`,`picture`,`preferred_language`,`background_check`) VALUES ('2','umesh','2020-03-26 14:32:54','umesh','2020-03-26 14:32:54','rakesh','kushwaha','/rakesh','english',2);
INSERT INTO `users` (`id`,`created_by`,`created_time`,`last_updated_by`,`last_updated_time`,`first_name`,`last_name`,`picture`,`preferred_language`,`background_check`) VALUES ('3','umesh','2020-03-26 14:33:10','umesh','2020-03-26 14:33:10','suresh','kushwaha','/suresh','english',2);
INSERT INTO `users` (`id`,`created_by`,`created_time`,`last_updated_by`,`last_updated_time`,`first_name`,`last_name`,`picture`,`preferred_language`,`background_check`) VALUES ('4','umesh','2020-03-26 14:33:37','umesh','2020-03-26 14:33:37','sujeet','kushwaha','/sujeet','english',2);
