package com.umesh.domain;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

public class Users implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	String id;
	String created_by;
	Date created_time;
	String lastUpdated_by;
	Date lastUpdated_time;
	String first_name;
	String last_name;
	String picture;
	String preferred_language;
	int background_check;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCreated_by() {
		return created_by;
	}
	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}
	public Date getCreated_time() {
		return created_time;
	}
	public void setCreated_time(Date created_time) {
		this.created_time = created_time;
	}
	public String getLastUpdated_by() {
		return lastUpdated_by;
	}
	public void setLastUpdated_by(String lastUpdated_by) {
		this.lastUpdated_by = lastUpdated_by;
	}
	public Date getLastUpdated_time() {
		return lastUpdated_time;
	}
	public void setLastUpdated_time(Date lastUpdated_time) {
		this.lastUpdated_time = lastUpdated_time;
	}
	public String getFirst_name() {
		return first_name;
	}
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}
	public String getLast_name() {
		return last_name;
	}
	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}
	public String getPicture() {
		return picture;
	}
	public void setPicture(String picture) {
		this.picture = picture;
	}
	public String getPreferred_language() {
		return preferred_language;
	}
	public void setPreferred_language(String preferred_language) {
		this.preferred_language = preferred_language;
	}
	public int getBackground_check() {
		return background_check;
	}
	public void setBackground_check(int background_check) {
		this.background_check = background_check;
	}
	
	
	
	
}
