package com.umesh.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.umesh.domain.Users;

@Mapper
public interface UserMapper {

	
	@Select("select * from users")
	List<Users> findAll();
	
	@Select("select * from users where id = #{id}")
	Users findById(@Param("id") String id );
	
	@Delete("DELETE FROM users WHERE id = #{id}")
	public int deleteById(String id);

	@Insert("INSERT INTO users(id, first_name, last_name,picture) VALUES (#{id}, #{first_name}, #{last_name}, #{picture})")
	public int insert(Users user);

	@Update("Update users set first_name=#{first_name} where id=#{id}")
	public int update(Users users);
}
