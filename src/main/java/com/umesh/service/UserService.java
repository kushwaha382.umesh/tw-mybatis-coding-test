package com.umesh.service;

import java.util.List;

import com.umesh.domain.Users;

public interface UserService {

	
	List<Users> findAll();
	
	
	Users findById( String id );
	
	
	public void deleteById(String id);

	
	public int insert(Users user);

	
	public int update(Users users);
}
