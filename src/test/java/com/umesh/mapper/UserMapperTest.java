package com.umesh.mapper;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.umesh.domain.Users;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserMapperTest {
	
	@Autowired
	private UserMapper userMapper;
	
	@Test
	public void getByIdTest() {
		Users s = userMapper.findById("1");
        System.out.println("student name:"+s.getFirst_name());
        assertNotNull(s);
	}
	
	@Test
	public void deleteByIdTest() {
		int s = userMapper.deleteById("1");
        assertEquals(1, s);
	}

}
